# Projet S7 - Logiciel de gestion de listes d'étudiants

### Cette application à été développée dans le cadre de notre projet de semestre 7 à Polytech Tours.
Cette application permet de récupérer des listes d'étudiants depuis des fichiers Excels avec un format spécial décrit dans le rapport ci-joint.

_Projet réalisé en binôme :  Kilian P. & Nathan C._
