package com.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;

public class OpeningView extends JFrame {
    private JPanel globalPanel; //
    private JTextField textFieldFile; //
    private JButton openAppButton; //
    private JButton FileChooserOpen; //

    /**
     * Constructeur par défaut de la vue d'ouverture de l'application
     *
     * @param name Le nom de la vue
     */
    public OpeningView(String name) {
        super(name);
        setContentPane(globalPanel);
    }

    /**
     * Fonction permettant l'ouverture de la vue
     */
    public void open() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setSize(600, 275);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Getter du field contenant le nom du ou des fichier(s) XLS
     *
     * @return Le JTextField
     */
    public JTextField getTextFieldFile() {
        return textFieldFile;
    }

    /**
     * Getter du bouton pour l'ouverture de l'application principale
     *
     * @return Le bouton
     */
    public JButton getOpenAppButton() {
        return openAppButton;
    }

    /**
     * Getter du bouton permettant l'ouverture du JFileChooser pour le choix du ou des fichier(s) XLS
     *
     * @return Le bouton
     */
    public JButton getFileChooserOpen() {
        return FileChooserOpen;
    }

}
