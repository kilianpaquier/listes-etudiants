package com.model;

import java.util.ArrayList;
import java.util.List;

public class Polytech {
    private List<FileAnalysis> fileAnalyses = new ArrayList<>(); // La liste des fichiers d'analyses

    /**
     * Constructeur
     */
    public Polytech() {

    }

    /**
     * Fonction ajoutant un fileAnalisys à la liste si il n'existe pas déjà
     * @param fileAnalysis Le fileAnalisys à ajouter
     */
    public void addFileAnalysis(FileAnalysis fileAnalysis) {
        if (getFileAnalysis(fileAnalysis.getDegree()) == null)
            fileAnalyses.add(fileAnalysis);
    }

    /**
     * Fonction qui retire un fileAnalysis de la liste
     * @param fileAnalysis Le fileAnalysis à retirer
     * @return Le booléen disant si le fileAnalysis a bien été supprimé ou non
     */
    public boolean removeFileAnalysis(FileAnalysis fileAnalysis) {
        return fileAnalyses.remove(fileAnalysis);
    }

    /**
     * Fonction retournant la liste des fileAnalisys
     * @return La liste des fileAnalisys
     */
    public List<FileAnalysis> getFileAnalyses() {
        return fileAnalyses;
    }

    /**
     * Fonction retournant un fileAnalisys en fonction du degré passé en paramètre
     * @param degree Le degré de l'année dont on veut le file analisys
     * @return Le fileAnalisys s'il existe
     */
    public FileAnalysis getFileAnalysis(Degree degree) {
        for (FileAnalysis fileAnalysis : fileAnalyses) {
            if (fileAnalysis.getDegree().equals(degree))
                return fileAnalysis;
        }
        return null;
    }
}
