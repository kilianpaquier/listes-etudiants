package com.model;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.awt.Color;

import java.io.*;
import java.util.*;

public abstract class Utils {

    private static int numericValueCounter = 0;
    private static FileInputStream fis;
    private static HSSFWorkbook workbook;
    private static HSSFSheet sheet;
    private static CellStyle all, mobiliteStyle, redoublantStyle, mundusStyle, titleStyle;
    private static Row row;
    private static Cell cell;
    private static int rowNum, colNum;

    /**
     * Méthode initialisant tous les paramètres d'exportation
     * @param file le fichier a ouvrir
     * @param sheetName le nom de la feuille a créer/modifier
     * @param table le tableau
     * @param sortStrategy la stratégie de tri
     * @param studentList la liste complète d'étudiants
     * @param exportList la liste d'étudiants triés à exporter
     * @throws IOException exception
     */
    private static void initExport(File file, String sheetName, ModelTable table, Map<String, Boolean> sortStrategy, List<Student> studentList, List<Student> exportList) throws IOException {
        /*
        On ouvre le fichier
         */
        fis = new FileInputStream(file);
        workbook = new HSSFWorkbook(fis);
        sheet = workbook.getSheet(sheetName);

        /*
        On récupère les paramètres d'exportations
         */
        Properties config = new Properties();
        config.loadFromXML(new FileInputStream(".\\resources\\parameters\\parameters.xml"));

        /*
        On récupère les couleurs d'exportation pour les étudiants particuliers
         */
        Color redoublantColor = Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("redoublant", "0"))).substring(2).toUpperCase());
        Color mobiliteColor = Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("mobilite", "0"))).substring(2).toUpperCase());
        Color mundusColor = Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("mundus", "0"))).substring(2).toUpperCase());

        /*
        On convertit les couleurs en couleurs apache POI pour l'exportation
         */
        HSSFPalette palette = workbook.getCustomPalette();
        HSSFColor hssfColorRedoublant = palette.findColor((byte) redoublantColor.getRed(), (byte) redoublantColor.getGreen(), (byte) redoublantColor.getBlue());
        HSSFColor hssfColorMobilite = palette.findColor((byte) mobiliteColor.getRed(), (byte) mobiliteColor.getGreen(), (byte) mobiliteColor.getBlue());
        HSSFColor hssfColorMundus = palette.findColor((byte) mundusColor.getRed(), (byte) mundusColor.getGreen(), (byte) mundusColor.getBlue());

        /*
        Si les couleurs définies dans les paramètres n'existent pas dans la palette de couleurs de HSSF alors on
        redéfini les couleurs
         */
        if (hssfColorRedoublant == null) {
            palette.setColorAtIndex(HSSFColor.HSSFColorPredefined.YELLOW.getIndex(),
                    (byte) redoublantColor.getRed(), (byte) redoublantColor.getGreen(), (byte) redoublantColor.getBlue());
            hssfColorRedoublant = palette.getColor(HSSFColor.HSSFColorPredefined.YELLOW.getIndex());
        }

        if (hssfColorMobilite == null) {
            palette.setColorAtIndex(HSSFColor.HSSFColorPredefined.BLUE_GREY.getIndex(),
                    (byte) mobiliteColor.getRed(), (byte) mobiliteColor.getGreen(), (byte) mobiliteColor.getBlue());
            hssfColorMobilite = palette.getColor(HSSFColor.HSSFColorPredefined.BLUE_GREY.getIndex());
        }

        if (hssfColorMundus == null) {
            palette.setColorAtIndex(HSSFColor.HSSFColorPredefined.GREEN.getIndex(),
                    (byte) mundusColor.getRed(), (byte) mundusColor.getGreen(), (byte) mundusColor.getBlue());
            hssfColorMundus = palette.getColor(HSSFColor.HSSFColorPredefined.GREEN.getIndex());
        }

        // Si la feuille d'exportation n'existe pas, on créé la feuille
        if (sheet == null) {
            sheet = workbook.createSheet(sheetName);
        }
        else{
            for (int i = sheet.getLastRowNum(); i >= 6; i--) {
                sheet.removeRow(sheet.getRow(i));
            }
        }

        /*
        Paramétrage du titre
         */
        HSSFFont titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 16);
        titleStyle = workbook.createCellStyle();
        titleStyle.setFont(titleFont);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        /*
        Paramétrage des contours de toutes les cellules
         */
        all = workbook.createCellStyle();
        all.setBorderTop(BorderStyle.MEDIUM);
        all.setBorderRight(BorderStyle.MEDIUM);
        all.setBorderBottom(BorderStyle.MEDIUM);
        all.setBorderLeft(BorderStyle.MEDIUM);
        all.setVerticalAlignment(VerticalAlignment.CENTER);


        /*
        Paramétrage des contours et de la couleur pour les cellules de mobilité
         */
        mobiliteStyle = workbook.createCellStyle();
        mobiliteStyle.setFillForegroundColor(hssfColorMobilite.getIndex());
        mobiliteStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        setStyles(mobiliteStyle);
        /*
        Paramétrage des contours et de la couleur pour les cellules du redoublement
         */
        redoublantStyle = workbook.createCellStyle();
        redoublantStyle.setFillForegroundColor(hssfColorRedoublant.getIndex());
        redoublantStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        setStyles(redoublantStyle);

        /*
        Paramétrage des contours et de la couleur pour les cellules des Mundus
         */
        mundusStyle = workbook.createCellStyle();
        mundusStyle.setFillForegroundColor(hssfColorMundus.getIndex());
        mundusStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        setStyles(mundusStyle);

        sort(table, sortStrategy, studentList, exportList);

        rowNum = 0;
        colNum = 0;

        rowNum += 5; // On écrit les étudiants à partir de la ligne 5
    }

    private static void setStyles(CellStyle style) {
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setBorderBottom(BorderStyle.MEDIUM);
        style.setBorderLeft(BorderStyle.MEDIUM);
        style.setBorderTop(BorderStyle.MEDIUM);
        style.setBorderRight(BorderStyle.MEDIUM);
    }

    /**
     * Cette fonction sert à parser un fichier xls pour récupérer toutes les informations sur les étudiants
     *
     * @param xlsFile Le fichier xls à parser
     * @return L'objet FileAnalysis qui va contenir toutes les information du fichier
     * @throws IOException un exception
     */
    public static FileAnalysis parseXLSFile(File xlsFile) throws IOException {
        FileAnalysis fileAnalysis = new FileAnalysis(); // On créé le fileAnalysis

        FileInputStream fis = new FileInputStream(xlsFile); // On ouvre le fichier
        HSSFWorkbook workbook = new HSSFWorkbook(fis); // On créé l'objet correspondant au fichier
        HSSFSheet sheet = workbook.getSheetAt(0); // On récupère la feuille zéro contenant la liste d'étudiant
        HSSFRow row;
        HSSFCell cell;

        Iterator rows = sheet.rowIterator(); // On fait un itérateur sur toutes les lignes de la feuille

        int index = 0; // On initialise un index qui va permette de savoir si on est à la ligne des noms de colonnes
        // ou à une ligne d'un étudiant

        /*
        On parcourt toutes les lignes
         */
        while (rows.hasNext()) {
            row = (HSSFRow) rows.next(); // On récupère la ligne

            /*
            On regarde sur quelle année on travaille et on set l'année du fileAnalysis
             */
            if (row.getCell(1) != null) {
                if (row.getCell(1).getStringCellValue().contains("3"))
                    fileAnalysis.setDegree(Degree.Third);
                if (row.getCell(1).getStringCellValue().contains("4"))
                    fileAnalysis.setDegree(Degree.Fourth);
                if (row.getCell(1).getStringCellValue().contains("5"))
                    fileAnalysis.setDegree(Degree.Fifth);
            }

            // On passe toutes les lignes vides
            if (row.getCell(0) == null || row.getCell(0).getCellTypeEnum().equals(CellType.BLANK))
                continue;

            /*
            Première ligne non vide
            On parcourt toutes les cellules existantes pour initialiser les colonnes
             */
            if (index == 0) {
                int columnId = 0;
                cell = row.getCell(columnId);

                while (cell != null && !cell.getCellTypeEnum().equals(CellType.BLANK)) {
                    while (columnId == 0 || columnId == 1 || columnId == 2)
                        columnId++;

                    cell = row.getCell(columnId);
                    Column column = new Column(cell.getStringCellValue(), columnId, null);
                    fileAnalysis.addColumn(column);
                    columnId++;
                    cell = row.getCell(columnId);
                }
            }

            /*
            A partir de la seconde ligne
             */
            else {
                // On créé un étudiant si la ligne n'est pas vide
                Student st = new Student("", "", "");

                /*
                On parcourt les cellules jusqu'au nombre de colonnes (+3 car on a pas ajouté les colonnes du numéro
                d'étudiant, du nom et du prénom à la liste des colonnes)
                 */
                for (int cellule = 0; cellule < fileAnalysis.getColumns().size() + 3; cellule++) {
                    cell = row.getCell(cellule); // On récupère la cellule

                    /*
                    On récupère le numéro de la colonne de la cellule
                     */
                    int columnIndex;
                    if (cell == null)
                        columnIndex = cellule;
                    else
                        columnIndex = cell.getColumnIndex();

                    /*
                    Si la cellule n'est pas nulle
                     */
                    if (cell != null) {
                        // Si c'est la colonne 0 alons on récupère le numéro d'étudiant
                        if (columnIndex == 0) {
                            String numero = String.valueOf(cell.getNumericCellValue());
                            numero = numero.substring(0, numero.length() - 2);
                            st.setStudentId(numero);
                            st.getCaracteristics().put(columnIndex, numero);
                        }

                        // Si c'est la colonne 1 alors on récupère le nom de famille
                        else if (columnIndex == 1) {
                            st.setLastName(cell.getStringCellValue());
                            st.getCaracteristics().put(columnIndex, cell.getStringCellValue().toUpperCase());
                        }

                        // Si c'est la colonne 2 alors on récupère le prénom
                        else if (columnIndex == 2) {
                            st.setFirstName(cell.getStringCellValue());
                            st.getCaracteristics().put(columnIndex, cell.getStringCellValue().toUpperCase());
                        }

                        /*
                        Si la cellule est de type string on ajoute une valeur aux caratéristiques de l'étudiant
                        Comme le programme est générique, on ne sait pas à quoi correspond la valeur en dehors de l'IHM
                        qui elle affichera le nom de la colonne avec la valeur
                         */
                        else if (cell.getCellTypeEnum() == CellType.STRING) {
                            fileAnalysis.getColumn(columnIndex).addValue(cell.getStringCellValue().toUpperCase());
                            st.getCaracteristics().put(columnIndex, cell.getStringCellValue().toUpperCase());
                        }
                        /*
                        Si la cellule est de type numérique on effectue le traitement suivant :
                         */
                        else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                            // si moins de 10 valeurs différentes, on laisse les valeures numériques
                            if (numericValueCounter <= 9 && !fileAnalysis.getColumn(columnIndex).getValues().contains("quantitatif")) {
                                st.getCaracteristics().put(columnIndex, String.valueOf(cell.getNumericCellValue()));
                                String numValue = String.valueOf(cell.getNumericCellValue());
                                if (!fileAnalysis.getColumn(columnIndex).addValue(numValue.substring(0, numValue.length() - 2)))
                                    numericValueCounter++;
                            }
                            /*
                            Si plus de 10 valeurs différentes pour la premiere fois, on vide la liste et on affiche seulement que le caractére est quantitatif
                            Les valeurs numériques sont toujours attribuées à l'objet représentant l'étudiant en question
                             */
                            else if (numericValueCounter > 9 && !fileAnalysis.getColumn(columnIndex).getValues().contains("quantitatif")) {
                                st.getCaracteristics().put(columnIndex, String.valueOf(cell.getNumericCellValue()));
                                fileAnalysis.getColumn(columnIndex).getValues().clear();
                                fileAnalysis.getColumn(columnIndex).addValue("quantitatif");
                            }
                            /*
                            Ajoute la valeur à l'étudiant mes laisse quantitatif dans le tableau d'affichage
                             */
                            else if (numericValueCounter > 9 && fileAnalysis.getColumn(columnIndex).getValues().contains("quantitatif")) {
                                st.getCaracteristics().put(columnIndex, String.valueOf(cell.getNumericCellValue()));
                            }
                        }

                        // Si la cellule est vide alors on ajoute une caractéristique nulle
                        else if (cell.getCellTypeEnum() == CellType.BLANK || cell.getCellTypeEnum() == CellType._NONE) {
                            st.getCaracteristics().put(columnIndex, "NONE");
                            if (!fileAnalysis.getColumn(columnIndex).getValues().contains("NONE"))
                                fileAnalysis.getColumn(columnIndex).addValue("NONE");
                        }
                    }
                    // Si la cellule est nulle alors on ajoute une caractéristique nulle
                    else {
                        st.getCaracteristics().put(columnIndex, "NONE");
                        if (!fileAnalysis.getColumn(columnIndex).getValues().contains("NONE"))
                            fileAnalysis.getColumn(columnIndex).addValue("NONE");
                    }
                }
                fileAnalysis.addStudent(st); // On ajoute l'édutiant à la liste des étudiants
            }
            index++; // On incrémente l'index RAPPEL : 0 -> On initialise les colonnes et != 0 On initialise un étudiant
        }

        /*
        On ferme le fichier
         */
        workbook.close();
        fis.close();

        fileAnalysis.setFile(xlsFile); // On set le fichier au fileAnalysis
        return fileAnalysis; // On retourne le fileAnalysis
    }

    /**
     * tri les étudiants à exporter
     *
     * @param table        le model de la table
     * @param sortStrategy la map pour trier les étudiants
     * @param studentList  la liste complète d'étudiants
     * @param exportList   la liste d'étudiant à exporter
     */
    private static void sort(ModelTable table, Map<String, Boolean> sortStrategy, List<Student> studentList, List<Student> exportList) {
        /*
        On récupère les données du tableau dans une map contenant la valeur de la colonne et le booléen vrai ou faux si
        un étudiant contenant cette valeur doit être exporté
         */
        for (int i = 0; i < table.getRowCount(); i++) {
            for (int j = 1; j < table.getColumnCount(); j += 2) {
                if (table.getValueAt(i, j - 1) != null)
                    sortStrategy.put(table.getValueAt(i, j - 1).toString(), Boolean.valueOf(table.getValueAt(i, j).toString()));
            }
        }

        /*
        On parcourt les étudiants un à un
         */
        for (Student student : studentList) {
            boolean sameValues = true;
            /*
            On parcourt la map du tableau
             */
            for (Map.Entry entry : sortStrategy.entrySet()) {
                // On regarde si le booléen de la valeur du tableau est sélectionnée et si l'étudiant ne contient pas la valeur dans ses caractéristiques
                if (entry.getValue() == Boolean.TRUE && !student.getCaracteristics().containsValue(entry.getKey().toString()))
                    sameValues = false; // Dans ce cas on "retire" l'étudiant pour l'exportation
            }

            /*
            Si la valeur est vraie, on met l'étudiant dans la liste d'exportation
             */
            if (sameValues)
                exportList.add(student);
        }
    }

    /**
     * Cette fonction sert à exporter un model de JTable dans une feuille xls définie par l'utilisateur
     *
     * @param table       Le model a exporter en xls
     * @param studentList La liste des étudiants à exporter
     * @param file        Le fichier xls où on va écrire (fihier contenant la liste complète des étudiants)
     * @param sheetName   La feuille dans la quelle on va écrire
     * @throws IOException une exception
     */
    public static void exportListToXLS(ModelTable table, List<Student> studentList, File file, String sheetName) throws IOException {

        /*
        On initialise les variables utiles
         */
        List<Student> exportList = new ArrayList<>();
        Map<String, Boolean> sortStrategy = new HashMap<>();
        List<String> colNames = new ArrayList<>();
        colNames.add("N° Carte");
        colNames.add("Nom");
        colNames.add("Prénom");

        initExport(file, sheetName, table, sortStrategy, studentList, exportList);

        /*
        On écrit les colonnes
         */
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        for (String colName : colNames) {
            cell = row.createCell(colNum);
            cell.setCellValue(colName);
            cell.setCellStyle(all);
            colNum++;
        }
        rowNum++;

        /*
        On écrit les étudiants selectionnés et on paint les rows selon le statut de l'étudiant (redoublant, mobilité, mundus)
        Si l'étudiant est redoublant et effectue une mobilité : la rangé prends la couleur dédiée à une mobilité (défaut bleu)
         */
        for (Student anExportList : exportList) {
            row = sheet.createRow(rowNum);
            if (anExportList.isMobilite())
                paintRowList(anExportList, row, mobiliteStyle);
            else if (anExportList.isRedoublant())
                paintRowList(anExportList, row, redoublantStyle);
            else if (anExportList.isMundus())
                paintRowList(anExportList, row, mundusStyle);

            else
                paintRowList(anExportList, row, all);
            rowNum++;
        }

        /*
        On écrit l'entête du fichier (année, nb élèves, ...)
         */
        paintTitle(sheet, file, titleStyle);

        /*
        On écrit la formule pour le nombre d'étudiants ainsi que le format pour la date, le professeur et la matière
         */
        paintForm(sheet, row);

        /*
        On ferme le fichier et on ouvre le fichier en écriture
         */
        fis.close();
        FileOutputStream fos = new FileOutputStream(file);

        /*
        On écrit dans le fichier et on ferme juste après
         */
        closeWorkbook(workbook, fos);
    }

    public static void exportNotesToXLS(ModelTable table, List<Student> studentList, File file, String sheetName) throws IOException {
         /*
        On initialise les variables utiles
         */
        List<Student> exportList = new ArrayList<>();
        Map<String, Boolean> sortStrategy = new HashMap<>();
        List<String> colNames = new ArrayList<>();
        colNames.add("N° Carte");
        colNames.add("Nom");
        colNames.add("Prénom");
        colNames.add("Note");

        initExport(file, sheetName, table, sortStrategy, studentList, exportList);

        /*
        On écrit les colonnes
         */
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        for (int i = 0; i < Math.ceil(exportList.size() / 30.0); i++) {
            for (String colName : colNames) {
                cell = row.createCell(colNum);
                cell.setCellValue(colName);
                cell.setCellStyle(all);
                colNum++;
            }
            colNum += 1;
        }
        rowNum++;

        /*
        On écrit les étudiants selectionnés et on paint les rows selon le statut de l'étudiant (redoublant, mobilité, mundus)
        Si l'étudiant est redoublant et effectue une mobilité : la rangé prends la couleur dédiée à une mobilité (défaut bleu)
         */
        for (Student anExportList : exportList) {
            if (rowNum - 5 <= 30) {
                row = sheet.createRow(rowNum);
                if (anExportList.isMobilite())
                    paintRowNote(anExportList, row, 0, mobiliteStyle);
                else if (anExportList.isRedoublant())
                    paintRowNote(anExportList, row, 0, redoublantStyle);
                else if (anExportList.isMundus())
                    paintRowNote(anExportList, row, 0, mundusStyle);

                else
                    paintRowNote(anExportList, row, 0, all);
            } else if ((rowNum - 5 > 30) && (rowNum - 5 <= 60)) {
                row = sheet.getRow(rowNum - 30);
                if (anExportList.isMobilite())
                    paintRowNote(anExportList, row, 5, mobiliteStyle);
                else if (anExportList.isRedoublant())
                    paintRowNote(anExportList, row, 5, redoublantStyle);
                else if (anExportList.isMundus())
                    paintRowNote(anExportList, row, 5, mundusStyle);

                else
                    paintRowNote(anExportList, row, 5, all);
            } else if ((rowNum - 5 > 60) && (rowNum - 5 <= 90)) {
                row = sheet.getRow(rowNum - 60);
                if (anExportList.isMobilite())
                    paintRowNote(anExportList, row, 8, mobiliteStyle);
                else if (anExportList.isRedoublant())
                    paintRowNote(anExportList, row, 8, redoublantStyle);
                else if (anExportList.isMundus())
                    paintRowNote(anExportList, row, 8, mundusStyle);

                else
                    paintRowNote(anExportList, row, 8, all);
            }
            rowNum++;
        }

        // On écrit l'entête de la feuille
        paintTitle(sheet, file, titleStyle);

        /*
        On ferme le fichier et on ouvre le fichier en écriture
         */
        fis.close();
        FileOutputStream fos = new FileOutputStream(file);

        /*
        On écrit dans le fichier et on ferme juste après
         */
        closeWorkbook(workbook, fos);
    }

    public static void exportListEmargement(ModelTable table, List<Student> studentList, File file, String sheetName) throws IOException{
        /*
        On initialise les variables utiles
         */
        List<Student> exportList = new ArrayList<>();
        Map<String, Boolean> sortStrategy = new HashMap<>();
        List<String> colNames = new ArrayList<>();
        colNames.add("N° Carte");
        colNames.add("Nom");
        colNames.add("Prénom");
        colNames.add("Date");
        colNames.add("Date");
        colNames.add("Date");
        colNames.add("Date");
        colNames.add("Date");

        initExport(file, sheetName, table, sortStrategy, studentList, exportList);

        exportList.add(0,new Student("","",""));

        /*
        On écrit les colonnes
         */
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        for (String colName : colNames) {
            cell = row.createCell(colNum);
            cell.setCellValue(colName);
            cell.setCellStyle(all);
            colNum++;
        }
        rowNum++;

        sheet.addMergedRegion(new CellRangeAddress(rowNum - 1, rowNum, 0,0));
        sheet.addMergedRegion(new CellRangeAddress(rowNum - 1, rowNum, 1,1));
        sheet.addMergedRegion(new CellRangeAddress(rowNum - 1, rowNum, 2,2));

        /*
        On écrit les étudiants selectionnés et on paint les rows selon le statut de l'étudiant (redoublant, mobilité, mundus)
        Si l'étudiant est redoublant et effectue une mobilité : la rangé prends la couleur dédiée à une mobilité (défaut bleu)
         */
        for (Student anExportList : exportList) {
            row = sheet.createRow(rowNum);
            if (anExportList.isMobilite())
                paintRowListEmargement(anExportList, row, mobiliteStyle);
            else if (anExportList.isRedoublant())
                paintRowListEmargement(anExportList, row, redoublantStyle);
            else if (anExportList.isMundus())
                paintRowListEmargement(anExportList, row, mundusStyle);

            else
                paintRowListEmargement(anExportList, row, all);
            rowNum++;
        }

        /*
        On écrit l'entête du fichier (année, nb élèves, ...)
         */
        paintTitle(sheet, file, titleStyle);

        /*
        On écrit la formule pour le nombre d'étudiants ainsi que le format pour la date, le professeur et la matière
         */
        paintForm(sheet, row);

        /*
        On ferme le fichier et on ouvre le fichier en écriture
         */
        fis.close();
        FileOutputStream fos = new FileOutputStream(file);

        /*
        On écrit dans le fichier et on ferme juste après
         */
        closeWorkbook(workbook, fos);
    }

    /**
     * Fonction permettant de surligner une ligne pour un étudiant avec une couleur précise pour l'export de liste
     *
     * @param st    L'étudiant
     * @param row   La ligne où est écrit l'étudiant dans le fichier
     * @param style Le style de la ligne
     */
    private static void paintRowList(Student st, Row row, CellStyle style) {
        Cell cell = row.createCell(0);
        cell.setCellValue(st.getStudentId());
        cell.setCellStyle(style);

        cell = row.createCell(1);
        cell.setCellValue(st.getFirstName());
        cell.setCellStyle(style);

        cell = row.createCell(2);
        cell.setCellValue(st.getLastName());
        cell.setCellStyle(style);
    }

    /**
     * Fonction permettant de surligner une ligne pour un étudiant avec une couleur précise pour l'export de liste
     *
     * @param st    L'étudiant
     * @param row   La ligne où est écrit l'étudiant dans le fichier
     * @param style Le style de la ligne
     */
    private static void paintRowListEmargement(Student st, Row row, CellStyle style) {
        paintRowList(st, row, style);

        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(style);

        cell = row.createCell(4);
        cell.setCellValue("");
        cell.setCellStyle(style);

        cell = row.createCell(5);
        cell.setCellValue("");
        cell.setCellStyle(style);

        cell = row.createCell(6);
        cell.setCellValue("");
        cell.setCellStyle(style);

        cell = row.createCell(7);
        cell.setCellValue("");
        cell.setCellStyle(style);
    }

    /**
     * Fonction permettant de surligner une ligne pour un étudiant avec une couleur précise pour l'export de notes
     *
     * @param st     L'étudiant
     * @param row    La ligne où est écrit l'étudiant
     * @param colNum Le numéro de la colonne (pour séparer en plusieurs colonnes si + de 30 élèves
     * @param style  Le style de la ligne
     */
    private static void paintRowNote(Student st, Row row, int colNum, CellStyle style) {

        Cell cell = row.createCell(colNum);
        cell.setCellValue(st.getStudentId());
        cell.setCellStyle(style);

        cell = row.createCell(colNum + 1);
        cell.setCellValue(st.getFirstName());
        cell.setCellStyle(style);

        cell = row.createCell(colNum + 2);
        cell.setCellValue(st.getLastName());
        cell.setCellStyle(style);

        cell = row.createCell(colNum + 3);
        cell.setCellValue("");
        cell.setCellStyle(style);
    }

    /**
     * Fonction permettant d'écrire l'entête de la feuille
     *
     * @param sheet      La feuille
     * @param file       Le fichier
     * @param titleStyle Le style du titre
     */
    private static void paintTitle(HSSFSheet sheet, File file, CellStyle titleStyle) {
        /*
        On écrit l'entête du fichier (année, nb élèves, ...)
         */
        row = sheet.createRow(0);
        cell = row.createCell(0);
        if (file.getName().contains("DI3"))
            cell.setCellValue("DI3");
        else if (file.getName().contains("DI4"))
            cell.setCellValue("DI4");
        else if (file.getName().contains("DI5"))
            cell.setCellValue("DI5");

        cell.setCellStyle(titleStyle);

        /*
        On écrit la formule pour le nombre d'étudiants ainsi que le format pour la date, le professeur et la matière
         */
        paintForm(sheet, row);
    }

    /**
     * Méthode qui écrit la formule pour le nombre d'étudiants ainsi que le format pour la date, le professeur et la matière
     *
     * @param sheet la feuille en cours
     * @param row   la ligne en cours
     */
    private static void paintForm(HSSFSheet sheet, Row row) {

        cell = row.createCell(1);
        cell.setCellType(CellType.FORMULA);
        cell.setCellFormula("COUNTA(A7:A200)");

        cell = row.createCell(2);
        cell.setCellType(CellType.STRING);
        cell.setCellValue("Elèves");

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Date : ");

        row = sheet.createRow(3);
        cell = row.createCell(0);
        cell.setCellValue("Matière : ");
        cell = row.createCell(2);
        cell.setCellValue("Professeur :");
    }

    /**
     * Méthode de fermeture du workbook et d'écriture dans le fichier
     * @param workbook le workbook
     * @param fos le stream de sortie
     */
    private static void closeWorkbook(HSSFWorkbook workbook, FileOutputStream fos) {
        try {
            workbook.write(fos);
            workbook.close();
            fos.close();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
