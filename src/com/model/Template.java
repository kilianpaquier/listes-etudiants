package com.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;

public class Template implements Serializable, Comparable<Template> {

    private static final long serialVersionUID = -5958140385086092802L; // Attribut serialUID permettant la sérialisation
    private String templateName; // Le nom du template
    private ModelTable templateModel; // Le modèle du template
    private Degree templateDegree; // Le degré du template

    /**
     * Constructeur prenant en paramètre le nom du template et un modèle
     * @param templateName Le nom du template
     * @param model Le modèle du template
     */
    public Template (String templateName, ModelTable model) {
        this.templateName = templateName;
        this.templateModel = model;
        this.templateDegree =  model.getDegree();
    }

    /**
     * Fonction permettant de sérialiser l'objet template selon les attributs que l'on veut écrire dans le fichier
     * @param stream L'ObjectOutputStream qui va écrire le template dans le fichier
     * @throws IOException Un exception
     */
    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeObject(templateDegree);
        stream.writeObject(templateName);
        stream.writeObject(templateModel.getDataVector());
        stream.writeObject(templateModel.getColumnNames());
    }

    /**
     * Fonction permettant de parser l'objet template à partir d'un stream
     * @param stream L'ObjectInputStream contenant les informations du template
     * @throws IOException Une exception
     * @throws ClassNotFoundException Si la classe n'est pas définie
     */
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        templateDegree = (Degree) stream.readObject();
        templateName = (String) stream.readObject();
        templateModel = new ModelTable();
        templateModel.setDataVector((Vector<Vector>) stream.readObject(), (List<String>) stream.readObject());
        templateModel.setDegree(templateDegree);
    }

    /**
     * Getter du modèle
     * @return Le modèle du template
     */
    public ModelTable getModel() {
        return templateModel;
    }

    /**
     * Getter du nom du template
     * @return Le nom du template
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Getter du degré du template
     * @return Le degré du template
     */
    public Degree getTemplateDegree() {
        return templateDegree;
    }

    /**
     * Setter du degré du template
     * @param templateDegree Le nouveau degré du template
     */
    public void setTemplateDegree(Degree templateDegree) {
        this.templateDegree = templateDegree;
    }

    /**
     * Setter du modèle du template
     * @param templateModel Le nouveau modèle du template
     */
    public void setTemplateModel(ModelTable templateModel) {
        this.templateModel = templateModel;
    }

    /**
     * Fonction comparant deux template
     * @param template Le template à comparer
     * @return 1 si les templates sont différents, 0 sinon
     */
    @Override
    public int compareTo(Template template) {
        for (Vector vectorLigne : template.getModel().getDataVector()) {
            if (!templateModel.getDataVector().contains(vectorLigne))
                return 1;
        }
        return 0;
    }
}
