package com.model;

import java.io.*;

abstract public class Serialize {

    /**
     * Méthode sérialisant un template dans son fichier de sérialisation
     * @param template Le template à sérialiser
     * @param serializationFile Le fichier de sérialisation
     */
    public static void serializeTemplate(Template template, File serializationFile) {
        try {
            FileOutputStream out = new FileOutputStream(serializationFile);
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(template);
            oos.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode chargeant un template depuis son fichier de sérialisation
     * @param serializationFile Le fichier de sérialisation
     * @return Le template sous sa forme d'objet
     */
    public static Template deserializeTemplate(File serializationFile) {
        try {
            FileInputStream in = new FileInputStream(serializationFile);
            ObjectInputStream ois = new ObjectInputStream(in);
            Template template = (Template) ois.readObject();
            ois.close();
            in.close();
            return template;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
