package com.controler;

import com.model.*;
import com.view.MainView;
import com.view.Notification;
import com.view.OpeningView;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.List;
import java.util.*;

public class AppControler {
    private MainView mainView; // La vue principale de l'application
    private Polytech polytech; // Le modèle de l'application
    private File currentPath; // Le chemin courant du jar afin d'accéder rapidement aux fichiers
    private File templateDirectory; // Le chemin vers le dossier des templates
    private Properties config = new Properties();

    /**
     * Constructeur prenant en paramètre un nom d'application
     *
     * @param title le nom de l'application
     */
    public AppControler(String title) {
        mainView = new MainView(title);
        polytech = new Polytech();
        templateDirectory = new File(".\\resources\\templates");
        UIManager.put("FileChooser.readOnly", Boolean.TRUE); // On règle les JFileChooser sur de la lecture only
    }

    public Polytech getPolytech() {
        return polytech;
    }

    MainView getMainView() {
        return mainView;
    }

    File getTemplateDirectory() {
        return templateDirectory;
    }

    Properties getConfig() {
        return config;
    }


    /**
     * Fonction d'ouverture de l'application
     */
    public void opening() {
        List<File> files = new ArrayList<>();
        OpeningView openingView = new OpeningView("Choix du ou des fichiers XLS - Liste d'étudiants");

        /*
        On créé les dossiers s'ils n'existent pas
        */
        new File(".\\resources").mkdir();
        new File(".\\resources\\templates").mkdir();
        new File(templateDirectory + "\\").mkdir();
        new File(templateDirectory + "\\DI3").mkdir();
        new File(templateDirectory + "\\DI4").mkdir();
        new File(templateDirectory + "\\DI5").mkdir();
        new File(".\\resources\\parameters").mkdir();
        try {
            if (!new File(".\\resources\\parameters\\parameters.xml").exists()) {
                new File(".\\resources\\parameters\\parameters.xml").createNewFile();
                initConfig();
            }
        } catch (IOException e) {
            new Notification(openingView, "Fichier initial des paramètres vide");
        }

        try {
            currentPath = new File(".").getCanonicalFile(); // On récupère le chemin courant
            config.loadFromXML(new FileInputStream(".\\resources\\parameters\\parameters.xml")); // On charge les paramètres du fichier xml
            /* On charge les fichiers par défault */
            List<File> defaultFiles = new ArrayList<>();
            defaultFiles.add(new File(config.getProperty("pathdi5")));
            defaultFiles.add(new File(config.getProperty("pathdi4")));
            defaultFiles.add(new File(config.getProperty("pathdi3")));
            for (File df : defaultFiles) {
                if (df.exists() && !df.getName().equalsIgnoreCase(" ")) {
                    files.add(df);
                }
            }
            openingView.getTextFieldFile().setText(files.toString());
        } catch (IOException e) {
            new Notification(openingView, e.getMessage());
        }

        openingView.getFileChooserOpen().addActionListener(e -> {
            // On restreint les fichiers pouvant être choisis à ceux au format XLS
            FileNameExtensionFilter filteFilter = new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls");

            /*
            On créé le JFileChooser (choix du fichier) et on lui assigne un certain nombre de paramètres
            */
            JFileChooser StudentFileChooser = new JFileChooser();
            StudentFileChooser.setDialogTitle("Choix du fichier XLS contenant la liste des étudiants"); // Le nom du JFileChooser
            StudentFileChooser.setMultiSelectionEnabled(true);
            StudentFileChooser.setCurrentDirectory(files.size() < 1 ? currentPath : files.get(0).getParentFile());
            StudentFileChooser.setAcceptAllFileFilterUsed(false); // On retire le choix "Tous les fichiers"
            StudentFileChooser.addChoosableFileFilter(filteFilter); // On ajoute le choix des fichiers XLS
            int value = StudentFileChooser.showOpenDialog(null); // On ouvre le JFileChooser
            if (value != JFileChooser.CANCEL_OPTION) { // On vérifie si l'utilisateur n'a pas annulé le choix
                files.clear();
                files.addAll(Arrays.asList(StudentFileChooser.getSelectedFiles())); // On récupère le fichier XLS choisi
                openingView.getTextFieldFile().setText(files.toString());
            }
        });

        openingView.getOpenAppButton().addActionListener(e -> {
            boolean allExists = true;
            if (files.size() == 0)
                new Notification(openingView, "Aucun fichier(s) sélectionné(s).");
            else {
                for (File file : files) {
                    if (!file.exists()) {
                        new Notification(openingView, "Le fichier : " + file.toString() + " n'existe pas.");
                        allExists = false;
                        break;
                    }
                }
                if (allExists) { // Si tous les fichiers existent
                    for (File file : files) {
                        try {
                            polytech.addFileAnalysis(Utils.parseXLSFile(file)); // On parse chaque fichier en objet
                            // fileAnalysis et on l'ajoute à la liste des fileAnalysis de polytech
                        } catch (IOException e1) {
                            new Notification(openingView, e1.getMessage());
                        }
                    }
                    mainView.open(); // On ouvre la vu principale
                    openingView.dispose(); // On ferme la vue d'ouverture
                    initComponents(); // On initialise les components
                    initActions(); // On initialise les actions
                }
            }
        });

        // Ouverture de l'application
        openingView.open();
    }

    private void initConfig() throws IOException {
        Properties properties = new Properties();
        properties.setProperty("mundusCol", "");
        properties.setProperty("mundusVal", "");
        properties.setProperty("mobiliteValDI5", "");
        properties.setProperty("mobiliteValDI4", "");
        properties.setProperty("mobiliteColDI4", "");
        properties.setProperty("mobiliteColDI5", "");
        properties.setProperty("redoubleColDI5", "");
        properties.setProperty("redoubleColDI4", "");
        properties.setProperty("redoubleColDI3", "");
        properties.setProperty("redoubleValDI3", "");
        properties.setProperty("redoubleValDI4", "");
        properties.setProperty("redoubleValDI3", "");
        properties.setProperty("redoubleValDI5", "");
        properties.setProperty("pathdi5", "");
        properties.setProperty("pathdi3", "");
        properties.setProperty("pathdi4", "");
        properties.setProperty("mundus", "-15420140");
        properties.setProperty("redoublant", "-205");
        properties.setProperty("mobilite", "-13408513");

        properties.storeToXML(new FileOutputStream(".\\resources\\parameters\\parameters.xml"), "Création");
    }

    /**
     * Fonction initialisant les components de l'application, modèles de bases pour listes et tableaux
     */
    private void initComponents() {
        setModelFileAnalysis(); // On initialise les JTable pour chaque fileAnalysis

        if (polytech.getFileAnalysis(Degree.Third) != null)
            mainView.getTabbedPane().setSelectedIndex(0);
        else if (polytech.getFileAnalysis(Degree.Fourth) != null)
            mainView.getTabbedPane().setSelectedIndex(1);
        else
            mainView.getTabbedPane().setSelectedIndex(2);

        InitControler.initPanelDI3(this);

        InitControler.initPanelDI4(this);

        InitControler.initPanelDI5(this);

        /*
        On charge les parameters du fichier xml

        les couleurs sont stockées dans le fichier sur forme d'integer représentant le rgb, on converti donc la valeur en hexadéimale
        dans la fonction Color.decode(String nm)
         */
        mainView.getRedoublantColorField().setBackground(Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("redoublant", "0"))).substring(2).toUpperCase()));
        mainView.getMobiliteColorField().setBackground(Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("mobilite", "0"))).substring(2).toUpperCase()));
        mainView.getMundusColorField().setBackground(Color.decode("#" + Integer.toHexString(Integer.valueOf(config.getProperty("mundus", "0"))).substring(2).toUpperCase()));

        /*
        Pour chaque fileAnalysis existant on créé le modèle de la liste des templates
         */
        if (polytech.getFileAnalysis(Degree.Fifth) != null)
            TemplateControler.setListModel(Degree.Fifth, this);
        if (polytech.getFileAnalysis(Degree.Fourth) != null)
            TemplateControler.setListModel(Degree.Fourth, this);
        if (polytech.getFileAnalysis(Degree.Third) != null)
            TemplateControler.setListModel(Degree.Third, this);
    }

    /**
     * Enregistre les paramètres à la fermeture de l'application selon les fichiers ouverts
     */
    public void closing() {
        AppControler controler = this;
        mainView.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                /*
                Sauvegarde des chemins des fichiers
                 */
                config.setProperty("pathdi3", (polytech.getFileAnalysis(Degree.Third) != null) ?
                        polytech.getFileAnalysis(Degree.Third).getFile().getPath() : " ");
                config.setProperty("pathdi4", (polytech.getFileAnalysis(Degree.Fourth) != null) ?
                        polytech.getFileAnalysis(Degree.Fourth).getFile().getPath() : " ");
                config.setProperty("pathdi5", (polytech.getFileAnalysis(Degree.Fifth) != null) ?
                        polytech.getFileAnalysis(Degree.Fifth).getFile().getPath() : " ");

                InitControler.closingDI3(controler);
                InitControler.closingDI4(controler);
                InitControler.closingDI5(controler);

                PropertiesControler.saveToXML(config);
            }
        });
    }

    /**
     * Fonction initialisant les actions de l'application
     */
    private void initActions() {
        mainView.getChangerDeFichierDI3().addActionListener(e -> {
            try {
                changeFile(Degree.Third);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getChangerDeFichierDI4().addActionListener(e -> {
            try {
                changeFile(Degree.Fourth);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        mainView.getChangerDeFichierDI5().addActionListener(e -> {
            try {
                changeFile(Degree.Fifth);
            } catch (IOException e1) {
                new Notification(mainView, e1.getMessage());
            }
        });

        /*
        Actions pour le tableau et les components de la 3e année
         */
        if (polytech.getFileAnalysis(Degree.Third) != null) {
            actionsDegree(Degree.Third);
        }

        /*
        Action pour les components de la 4e année
         */
        if (polytech.getFileAnalysis(Degree.Fourth) != null) {
            actionsDegree(Degree.Fourth);
        }

        // Actions pour les components de la 5e année
        if (polytech.getFileAnalysis(Degree.Fifth) != null) {
            actionsDegree(Degree.Fifth);
        }

        /*
        On enregistre la couleur définir pour les redoublants dans le fichier xml des paramètres
         */
        mainView.getRedoublantColorButton().addActionListener(e -> {
            Color color = JColorChooser.showDialog(null, "Choix d'une couleur pour les redoublants", new Color(255, 255, 51));
            if (color == null)
                mainView.getRedoublantColorField().setBackground(mainView.getRedoublantColorField().getBackground());
            else
                mainView.getRedoublantColorField().setBackground(color);
            config.setProperty("redoublant", String.valueOf(mainView.getRedoublantColorField().getBackground().getRGB()));
            try {
                config.storeToXML(new FileOutputStream(".\\resources\\parameters\\parameters.xml"), "Sauvegardé");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        /*
        On enregistre la couleur définit pour la mobilité dans le fichier xml des paramètres
         */
        mainView.getMobiliteColorButton().addActionListener(e -> {
            Color color = JColorChooser.showDialog(null, "Choix d'une couleur pour les étudiants en mobilité", new Color(51, 102, 255));
            if (color == null)
                mainView.getMobiliteColorField().setBackground(mainView.getMobiliteColorField().getBackground());
            else
                mainView.getMobiliteColorField().setBackground(color);
            config.setProperty("mobilite", String.valueOf(mainView.getMobiliteColorField().getBackground().getRGB()));
            try {
                config.storeToXML(new FileOutputStream(".\\resources\\parameters\\parameters.xml"), "Sauvegardé");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        /*
        On enregistre la couleur définit pour les Mundus dans le fichier xml des paramètres
         */
        mainView.getMundusColorButton().addActionListener(e -> {
            Color color = JColorChooser.showDialog(null, "Choix d'une couleur pour les Mundus", new Color(20, 181, 20));
            if (color == null)
                mainView.getMundusColorField().setBackground(mainView.getMundusColorField().getBackground());
            else
                mainView.getMundusColorField().setBackground(color);
            config.setProperty("mundus", String.valueOf(mainView.getMundusColorField().getBackground().getRGB()));
            try {
                config.storeToXML(new FileOutputStream(".\\resources\\parameters\\parameters.xml"), "Sauvegardé");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        closing();
    }

    /**
     * Fonction définissant toutes les actions pour un panel selon le degré passant en paramètre
     *
     * @param degree Le degré (année)
     */
    private void actionsDegree(Degree degree) {
        if (degree.equals(Degree.Third)) {
            ActionTableControler.actionTableDI3(this);
            ExportControler.exportDI3(this);
            TemplateControler.actionTemplatesDI3(this);
            PropertiesControler.saveSettingsDI3(this);
            actionsFieldBox(Degree.Third, mainView.getFieldDI3());
        }
        if (degree.equals(Degree.Fourth)) {
            ActionTableControler.actionTableDI4(this);
            ExportControler.exportDI4(this);
            TemplateControler.actionTemplatesDI4(this);
            PropertiesControler.saveSettingsDI4(this);
            actionsFieldBox(Degree.Fourth, mainView.getFieldDI4());
        }
        if (degree.equals(Degree.Fifth)) {
            ActionTableControler.actionTableDI5(this);
            ExportControler.exportDI5(this);
            TemplateControler.actionTemplatesDI5(this);
            PropertiesControler.saveSettingsDI5(this);
            actionsFieldBox(Degree.Fifth, mainView.getFieldDI5());
        }
        actionMouseHover();
    }

    /**
     * Fonction créant le modèle initial de chaque tableau des fileAnalysis*
     */
    private void setModelFileAnalysis() {
        // On parcours chaque fileAnalysis
        for (FileAnalysis fileAnalysis : polytech.getFileAnalyses()) {
            setModelDegreeFileAnalysis(fileAnalysis.getDegree());
        }
    }

    /**
     * Fonction qui créé le modèle pour un tableau en fonction de degré passé en paramètre
     *
     * @param degree Le degré (année)
     */
    private void setModelDegreeFileAnalysis(Degree degree) {
        FileAnalysis fileAnalysis = polytech.getFileAnalysis(degree);
        ModelTable modelTable = new ModelTable(); // On créé le modèle
        modelTable.setDegree(degree); // On set le degré du modèle

        // On parcours chaque colonne du fileAnalysis
        for (Column column : fileAnalysis.getColumns()) {
            Vector<Boolean> dataChooser = new Vector<>(); // On créé la colonne des CheckBox
            Vector<String> columnData = new Vector<>(column.getValues()); // On créé la colonne des données et on ajoute les données
            for (String ignored : columnData) {
                dataChooser.add(false); // On met à false toutes les sélections
            }

            // On ajoute la colonne des données et la colonne des sélections au modèle
            modelTable.addColumn(column.getColumnName(), columnData);
            modelTable.addColumn("", dataChooser);
        }

            /*
            On set le modèle en fonction du degré du tableau créé
             */
        if (degree.equals(Degree.Third))
            mainView.getTableDI3().setModel(modelTable);
        if (degree.equals(Degree.Fourth))
            mainView.getTableDI4().setModel(modelTable);
        if (degree.equals(Degree.Fifth))
            mainView.getTableDI5().setModel(modelTable);
    }

    /**
     * Fonction permettant d'initialiser une comboBox avec tous les noms de feuilles
     *
     * @param degree Le degré du fichier à charger
     * @return La comboBox contenant les noms de feuilles
     * @throws IOException Une exception
     */
    private JComboBox<String> initSuggestedLeaf(Degree degree) throws IOException {
        JComboBox<String> comboBox = new JComboBox<String>() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(super.getPreferredSize().width, 0); // On défini la dimension que l'on veut
            }
        };
        File fileAnalisys = polytech.getFileAnalysis(degree).getFile(); // On récupère le fichier

        /*
        On charge le fichier xls
         */
        FileInputStream fileInputStream = new FileInputStream(fileAnalisys);
        HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

        // On parcours toutes les feuilles et on ajoute le nom de la feuille à la comboBox
        for (int nbSheet = 0; nbSheet < workbook.getNumberOfSheets(); nbSheet++) {
            comboBox.addItem(workbook.getSheetName(nbSheet));
        }

        /*
        On ferme le fichier et on retourne la comboBox
         */
        fileInputStream.close();
        return comboBox;
    }

    /**
     * Fonction retournant la liste des items d'une comboBox
     *
     * @param comboBox La comboBox dont on veut récupérer les items
     * @return La liste des items
     */
    private List<String> getItems(JComboBox<String> comboBox) {
        List<String> items = new ArrayList<>();
        for (int nbItem = 0; nbItem < comboBox.getItemCount(); nbItem++) {
            items.add(comboBox.getItemAt(nbItem));
        }
        return items;
    }

    /**
     * Fonction de l'action pour un JtextField
     *
     * @param degree Le degré de la JtextField
     * @param field  Le field contenant le nom de la feuille
     */
    private void actionsFieldBox(Degree degree, JTextField field) {
        // On récupère le JtextField et on créé la comboBox
        JComboBox<String> comboBox = new JComboBox<>();

        // On initialise la comboBox avec les noms des feuilles du fichier
        try {
            comboBox = initSuggestedLeaf(degree);
        } catch (IOException e1) {
            new Notification(mainView, e1.getMessage());
        }
        assert comboBox != null; // On vérifie que la comboBox n'est pas null

        /*
        On récupère les items, on vide la comboBox, on définie la comboBox finale pour les actions et on set
        le font de l'affichage de la box
         */
        List<String> items = getItems(comboBox);
        comboBox.removeAllItems();
        JComboBox<String> finalComboBox = comboBox;
        finalComboBox.setFont(new Font(null, Font.PLAIN, 13));

        /*
        On set le bordLayout du JtextField pour bien afficher la comboBox, on ajoute la comboBox au borderLayout
        On retire l'action de VK_TAB car on va la redéfinir plus tard
         */
        field.setLayout(new BorderLayout());
        field.add(finalComboBox, BorderLayout.SOUTH);
        field.setFocusTraversalKeysEnabled(false);

        field.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DOWN) { // Si l'action est la flèche du bas
                    if (finalComboBox.isPopupVisible()) { // On vérifie que la popup est visible
                        if (finalComboBox.getSelectedItem() == null)
                            finalComboBox.setSelectedIndex(0); // Si aucun item est sélectionné on défini l'item sur le premier
                        else if (finalComboBox.getSelectedIndex() != finalComboBox.getItemCount() - 1)
                            finalComboBox.setSelectedIndex(finalComboBox.getSelectedIndex() + 1); // Sinon on prend l'item après celui déjà sélectionné
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_UP) { // Si l'action est la flèche du haut
                    if (finalComboBox.isPopupVisible()) { // On vérifie que la popup est visible
                        if (finalComboBox.getSelectedItem() == null)
                            finalComboBox.setSelectedIndex(0); // Si aucun item sélectionné on défini le premier item
                        else if (finalComboBox.getSelectedIndex() != 0)
                            finalComboBox.setSelectedIndex(finalComboBox.getSelectedIndex() - 1); // Sinon on prend l'item du dessus
                    }
                } else {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) { // Si l'action est un enter ou une tabulation
                        if (finalComboBox.isPopupVisible()) { // On vérifie que la popup est visible
                            if (finalComboBox.getSelectedItem() != null) { // On vérifie qu'un item est sélectionné
                                field.setText(String.valueOf(finalComboBox.getSelectedItem())); // On set le text du JTextField sur la valeur sélectionnée
                            }
                        }
                    }
                    updateLeaves(items, finalComboBox, field); // Si l'action est autre de toutes les précédentes
                    // cela veut dire que l'utilisateur tape des lettres donc on met à jour la liste commençant par le texte saisi
                }
            }
        });
    }

    /**
     * Fonction qui met à jour la liste des items
     *
     * @param items    La liste des items déjà présentes
     * @param comboBox La comboBox des items
     * @param field    Le field contenant le début du nom de la feuille
     */
    private void updateLeaves(List<String> items, JComboBox<String> comboBox, JTextField field) {
        /*
        Initialisation des variables utiles
         */
        List<String> list = new ArrayList<>(items);
        comboBox.removeAllItems();

        for (String item : items) { // On parcours tous les items
            if (!item.toLowerCase().startsWith(field.getText().toLowerCase())) // Si l'item ne commence pas par le texte actuel, on le retire de la liste
                list.remove(item);
            if (item.equalsIgnoreCase(field.getText())) // Si l'item est exactement égale au texte on le retire aussi
                list.remove(item);
        }

        // On ajoute les items restants à la comboBox
        for (String string : list) {
            comboBox.addItem(string);
        }
        field.remove(comboBox); // On retire la comboBox du JtextField

        int size = field.getText().length();
        // Si la liste n'est vide et que la taille du texte est supérieure à 0
        if (size != 0 && !list.isEmpty()) {
            field.add(comboBox, BorderLayout.SOUTH); // On ajoute la comboBox au borderLayout
            comboBox.setFont(new Font(null, Font.PLAIN, 13)); // On set la taille du texte de la combobox
            comboBox.setPopupVisible(true); // On rend la comboBox visible
        }
    }

    /**
     * Action définissant le changement de la forme de la souris pour la liste des templates et le tableau
     */
    private void actionMouseHover() {
        mainView.getListTemplateDI3().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getListTemplateDI3().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });

        mainView.getTableDI3().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getTableDI3().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });

        mainView.getListTemplateDI4().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getListTemplateDI4().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });

        mainView.getListTemplateDI5().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getListTemplateDI5().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });

        mainView.getTableDI4().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getTableDI4().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });

        mainView.getTableDI5().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mainView.getTableDI5().setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        });
    }

    /**
     * Fonction permettant de changer le fichier ouvert pour un panel, en fonction de degré passé en paramètre
     *
     * @param degree Le degré (année)
     * @throws IOException Un exception si le fichier XLS n'est pas conforme à l'année du panel, ou alors si l'utilisateur arrête la sélection
     */
    private void changeFile(Degree degree) throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls"));
        fileChooser.setDialogTitle("Choix du fichier XLS contenant la liste des étudiants");


        if (degree.equals(Degree.Third)) {
            if (new File(mainView.getLabelFichierDI3().getText()).exists())
                fileChooser.setCurrentDirectory(new File(mainView.getLabelFichierDI3().getText()).getParentFile());
            else
                fileChooser.setCurrentDirectory(polytech.getFileAnalyses().get(0).getFile());
            int value = fileChooser.showOpenDialog(null);
            if (value == JFileChooser.CANCEL_OPTION)
                throw new IOException("Arrêt du changement de fichier");

            FileAnalysis fileAnalysis = Utils.parseXLSFile(fileChooser.getSelectedFile());
            if (!fileAnalysis.getDegree().equals(Degree.Third))
                throw new IOException("Vous tentez d'importer un fichier qui ne correspond pas à la troisième année");

            boolean existed = polytech.removeFileAnalysis(polytech.getFileAnalysis(Degree.Third));
            polytech.addFileAnalysis(fileAnalysis);
            setModelDegreeFileAnalysis(Degree.Third);
            InitControler.initPanelDI3(this);
            if (!existed)
                actionsDegree(Degree.Third);

            mainView.getLabelFichierDI3().setText(fileAnalysis.getFile().toString());
        }

        if (degree.equals(Degree.Fourth)) {
            if (new File(mainView.getLabelFileDI4().getText()).exists())
                fileChooser.setCurrentDirectory(new File(mainView.getLabelFileDI4().getText()).getParentFile());
            else
                fileChooser.setCurrentDirectory(polytech.getFileAnalyses().get(0).getFile());
            int value = fileChooser.showOpenDialog(null);
            if (value == JFileChooser.CANCEL_OPTION)
                throw new IOException("Arrêt du changement de fichier");

            FileAnalysis fileAnalysis = Utils.parseXLSFile(fileChooser.getSelectedFile());
            if (!fileAnalysis.getDegree().equals(Degree.Fourth))
                throw new IOException("Vous tentez d'importer un fichier qui ne correspond pas à la quatrième année");

            boolean existed = polytech.removeFileAnalysis(polytech.getFileAnalysis(Degree.Fourth));
            polytech.addFileAnalysis(fileAnalysis);
            setModelDegreeFileAnalysis(Degree.Fourth);
            InitControler.initPanelDI4(this);
            if (!existed)
                actionsDegree(Degree.Fourth);

            mainView.getLabelFileDI4().setText(fileAnalysis.getFile().toString());
        }

        if (degree.equals(Degree.Fifth)) {
            if (new File(mainView.getLabelFileDI5().getText()).exists())
                fileChooser.setCurrentDirectory(new File(mainView.getLabelFileDI5().getText()).getParentFile());
            else
                fileChooser.setCurrentDirectory(polytech.getFileAnalyses().get(0).getFile());
            int value = fileChooser.showOpenDialog(null);
            if (value == JFileChooser.CANCEL_OPTION)
                throw new IOException("Arrêt du changement de fichier");

            FileAnalysis fileAnalysis = Utils.parseXLSFile(fileChooser.getSelectedFile());
            if (!fileAnalysis.getDegree().equals(Degree.Fifth))
                throw new IOException("Vous tentez d'importer un fichier qui ne correspond pas à la cinquième année");

            boolean existed = polytech.removeFileAnalysis(polytech.getFileAnalysis(Degree.Fifth));
            polytech.addFileAnalysis(fileAnalysis);
            setModelDegreeFileAnalysis(Degree.Fifth);
            InitControler.initPanelDI5(this);
            if (!existed)
                actionsDegree(Degree.Fifth);

            mainView.getLabelFileDI5().setText(fileAnalysis.getFile().toString());
        }
    }
}
