package test;

import com.model.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class FileAnalysisTest {
    private FileAnalysis fileAnalysis;

    @Before
    public void setUp() throws IOException {
        fileAnalysis = new FileAnalysis();

        fileAnalysis.setDegree(Degree.Fourth);

        // Columns
        fileAnalysis.addColumn(new Column("S impair", 1, null));
        fileAnalysis.addColumn(new Column("S Pair", 2, null));
        fileAnalysis.addColumn(new Column("Année", 3, null));
        fileAnalysis.addColumn(new Column("mobilité", 4, null));
        //fileAnalysis.addColumn(new Column("Colonne 5", 5, null));

        // To test research
        fileAnalysis.addTemplate(new Template("testTemplate", new ModelTable()));
        fileAnalysis.addColumn(new Column("Colonne", 9, null));
        fileAnalysis.addStudent(new Student("testFirst", "testLast", "50000"));
    }

    @Test
    public void testResearchColumnWithColumnNumber() {
        assertEquals(fileAnalysis.getColumn(1).getColumnName(), "S impair");
        assertEquals(fileAnalysis.getColumn(2).getColumnName(), "S Pair");
        assertEquals(fileAnalysis.getColumn(3).getColumnName(), "Année");
        assertEquals(fileAnalysis.getColumn(4).getColumnName(), "mobilité");
        //assertEquals(fileAnalysis.getColumn(5).getColumnName(), "Colonne 5");
    }

    @Test
    public void testResearchWithColumnName() {
        assertEquals(fileAnalysis.getColumn("S impair").getColumnNumber(), 1);
        assertEquals(fileAnalysis.getColumn("S Pair").getColumnNumber(), 2);
        assertEquals(fileAnalysis.getColumn("Année").getColumnNumber(), 3);
        assertEquals(fileAnalysis.getColumn("mobilité").getColumnNumber(), 4);
    }

    @Test
    public void testParseXLS() {
        FileAnalysis fileAnalysis = null;
        try {
            fileAnalysis = Utils.parseXLSFile(new File("test\\ResultatsDI4.xls"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(fileAnalysis);

        assertEquals(fileAnalysis.getDegree(),this.fileAnalysis.getDegree());

        int index = 0;

        for (Column column : this.fileAnalysis.getColumns()) {
            assertEquals(this.fileAnalysis.getColumns().get(index).getColumnName(), column.getColumnName());
            index++;
        }
    }

    @Test
    public void testAddTemplate() {
        try {
            fileAnalysis.addTemplate(new Template("testTemplate", new ModelTable()));
            fail();
        } catch (IOException e) {
            System.err.println("Test unitaire validé, exception bien levée fonction : testAddTemplate()");
        }
    }

    @Test
    public void testResearchTemplate() {
        assertNotNull(fileAnalysis.getTemplate("testTemplate"));
        assertNull(fileAnalysis.getTemplate("..."));
    }

    @Test
    public void testResearchStudent() {
        assertNotNull(fileAnalysis.getStudent("50000"));
        assertNull(fileAnalysis.getStudent("0"));
    }
}