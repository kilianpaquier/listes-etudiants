package test;

import com.model.ModelTable;
import com.model.Serialize;
import com.model.Template;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class TemplateTest {
    private Template template;

    @Before
    public void setUp() {
        template = new Template("testTemplate", new ModelTable());
    }

    @Test
    public void compareTo() {
        Template template2 = new Template("testTemplate", new ModelTable());
        assertEquals(0, template.compareTo(template2));
    }

    @Test
    public void testSerializeDeserialize() {
        Serialize.serializeTemplate(template, new File("templateTest"));
        Template templateDeseria = Serialize.deserializeTemplate(new File("templateTest"));
        assertEquals(0, template.compareTo(templateDeseria));
        new File("templateTest").delete();
    }
}